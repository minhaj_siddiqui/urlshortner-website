export class TinyUrlModel {
    id: number;
    longUrl: string;
    shortUrl: string;
}
