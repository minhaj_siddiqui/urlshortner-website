import { TestBed } from '@angular/core/testing';

import { UrlshortnerServiceService } from './urlshortner-service.service';

describe('UrlshortnerServiceService', () => {
  let service: UrlshortnerServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UrlshortnerServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
