import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders } from '@angular/common/http';
import { URLSHORTNER_HOST } from '../environments/environment'
import { Observable } from 'rxjs';
import { TinyUrlModel } from './tiny-url-model';

@Injectable({
  providedIn: 'root'
})

export class UrlshortnerServiceService {

  private create_uri = '/create'  

  constructor(private http: HttpClient) { }

  public createTinyURL(tinyUrlModel: TinyUrlModel): Observable<TinyUrlModel>  {
    var payload = {
      "LongURL": tinyUrlModel.longUrl
    }

    var url = URLSHORTNER_HOST + this.create_uri;

    return this.http.post<TinyUrlModel>(url, tinyUrlModel);
  }

}
