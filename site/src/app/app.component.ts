// app.component.ts
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UrlshortnerServiceService } from './urlshortner-service.service';
import { Observable, Subscriber } from 'rxjs';
import { NgForm } from '@angular/forms';
import { TinyUrlModel } from './tiny-url-model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'URLShortner Service';
  // angForm: FormGroup;
  shortURL: string;
  longUrl: string;

  constructor(private fb: FormBuilder, private urlShortnerService: UrlshortnerServiceService) {
    // this.initForm();
  }

  // initForm() {
  //   this.angForm = this.fb.group({
  //     longUrl: ['', Validators.required]
  //   });
  // }

  onCreateShortURL() {
    // Process checkout data here
    var tinyUrlModel = new TinyUrlModel();
    tinyUrlModel.longUrl = this.longUrl;

    this.shortURL = "Please wait....."

    this.urlShortnerService.createTinyURL(tinyUrlModel)      
      .subscribe(tinyUrlResponse => {
        // For now we are just replacing localhost with the current host of aws instance.
        // TODO: We need a better approach than than this, which is possible once we have domain name.

        if (tinyUrlResponse.shortUrl != "" && tinyUrlResponse.shortUrl.startsWith("http://localhost"))
        {
          tinyUrlResponse.shortUrl  = tinyUrlResponse.shortUrl.replace("localhost", window.location.hostname)
        }

        this.shortURL = tinyUrlResponse.shortUrl;

        
      },
      err=> this.shortURL = "Something went wrong please try again later.")
  }
}
